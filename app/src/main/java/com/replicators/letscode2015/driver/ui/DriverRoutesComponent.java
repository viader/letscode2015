package com.replicators.letscode2015.driver.ui;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.driver.adapter.PointListAdapter;
import com.replicators.letscode2015.driver.listener.DriverRoutesComponentListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by taiga on 07.11.16.
 */

public class DriverRoutesComponent extends LinearLayout {
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.plus)
    ImageView imageViewAdd;
    @Bind(R.id.minus)
    ImageView imageViewDelete;

    final List<String> pointsName = new ArrayList<>();

    final PointListAdapter pointListAdapter = new PointListAdapter(pointsName);

    DriverRoutesComponentListener listener = new DriverRoutesComponentListener() {
        @Override
        public void onPointsNumberChanged(int pointsNumber) {

        }
    };

    public DriverRoutesComponent(Context context) {
        super(context);
        init();
    }

    public DriverRoutesComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public DriverRoutesComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.component_driver_route_points, this);
        ButterKnife.bind(this);
        pointsName.add("Punkt startowy ");
        pointsName.add("Punkt końcowy ");
        imageViewAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                addPoint();
                listener.onPointsNumberChanged(pointsName.size());
            }
        });
        imageViewDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pointsName.size() > 2) {
                    deletePoint();
                    listener.onPointsNumberChanged(pointsName.size());
                }
            }
        });

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pointListAdapter);
    }

    private void addPoint() {
        pointsName.add(pointsName.size() - 1, "Punkt " + Character.toString((char) (pointsName.size() + 63)));
        imageViewDelete.setVisibility(VISIBLE);
        pointListAdapter.notifyDataSetChanged();
    }

    private void deletePoint() {
        pointsName.remove((pointsName.size() - 2));
        pointListAdapter.notifyDataSetChanged();
        if (pointsName.size() == 2)
            imageViewDelete.setVisibility(GONE);
    }

    public void setupPointsNumber(int pointsNumber) {
        while (pointsName.size() > pointsNumber) {
            deletePoint();
        }
        while (pointsName.size() < pointsNumber) {
            addPoint();
        }
    }


    public void setListener(DriverRoutesComponentListener listener) {
        this.listener = listener;
    }

}
