package com.replicators.letscode2015.driver.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.driver.activity.DriverDetailsActivity;
import com.replicators.letscode2015.base.api.domain.RestRoute;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by taiga on 20.10.16.
 */

public class RouteListAdapter extends RecyclerView.Adapter<RouteListAdapter.ViewHolder> {

    private List<RestRoute> routeInfoDataList;
    private BaseActivity activity;

    public RouteListAdapter(List<RestRoute> routeInfoDataList, BaseActivity activity) {
        this.routeInfoDataList = routeInfoDataList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup itemView, int viewType) {
        View view = LayoutInflater.from(itemView.getContext()).inflate(R.layout.route_list_item, itemView, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final RestRoute data = routeInfoDataList.get(position);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                Intent intent = new Intent(activity, DriverDetailsActivity.class);
                intent.putExtra(C.IntentKeys.REST_ROUTE, data);
                activity.startActivityForResult(intent, C.RequestCodes.NEW_FLOW);
            }
        });

        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
        String timeStart = localDateFormat.format(data.getStartTime());
        String timeEnd = localDateFormat.format(data.getEndTime());

        holder.routeName.setText(data.getRouteName().toUpperCase());
        holder.routeTime.setText(timeStart + " - " + timeEnd);
    }

    @Override
    public int getItemCount() {
        return routeInfoDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.main_layout)
        View mainLayout;

        @Bind(R.id.route_name)
        TextView routeName;

        @Bind(R.id.route_time)
        TextView routeTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
