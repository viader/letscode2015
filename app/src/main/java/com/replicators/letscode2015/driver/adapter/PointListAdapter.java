package com.replicators.letscode2015.driver.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.replicators.letscode2015.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by taiga on 20.10.16.
 */

public class PointListAdapter extends RecyclerView.Adapter<PointListAdapter.ViewHolder> {
    private List<String> pointsData;

    public PointListAdapter(List<String> pointsData) {
        this.pointsData = pointsData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup itemView, int viewType) {
        View view = LayoutInflater.from(itemView.getContext()).inflate(R.layout.point_list_item, itemView, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String data = pointsData.get(position);
        holder.pointName.setText(data);
    }

    @Override
    public int getItemCount() {
        return pointsData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.point_name)
        TextView pointName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
