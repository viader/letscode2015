package com.replicators.letscode2015.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.driver.adapter.RouteListAdapter;
import com.replicators.letscode2015.base.ui.MapRouteComponent;
import com.replicators.letscode2015.base.api.domain.RestPoint;
import com.replicators.letscode2015.base.api.domain.RestRoute;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverDetailsActivity extends BaseActivity {

    @Bind(R.id.route_name)
    TextView nameTextView;
    @Bind(R.id.describe)
    TextView describeTextView;
    @Bind(R.id.hours)
    TextView hoursTextView;
    @Bind(R.id.free_places)
    TextView freePlacesTextView;
    @Bind(R.id.activity_driver_details_edit)
    View buttonEdit;
    @Bind(R.id.activity_driver_details_delete)
    View buttonDelete;
    RestRoute route = new RestRoute();
    RestPoint restPoint = new RestPoint();
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    @Bind(R.id.map)
    MapRouteComponent mapRouteComponent;
    RouteListAdapter routeListAdapter;

    private ArrayList<RestRoute> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_details);
        ButterKnife.bind(this);
        mapRouteComponent.initMap(getSupportFragmentManager(), new ArrayList<MarkerOptions>());
        route = (RestRoute) getIntent().getSerializableExtra(C.IntentKeys.REST_ROUTE);
        nameTextView.setText(route.getRouteName().toUpperCase());
        describeTextView.setText(route.getDescribe());
        freePlacesTextView.setText(Integer.toString(route.getFreePlaces()) + " wolne miejsca");
        hoursTextView.setText(sdf.format(route.getStartTime()) + "-" + sdf.format(route.getEndTime()));
        routeListAdapter = new RouteListAdapter(list, this);

        ArrayList<MarkerOptions> initMarkersOptions = new ArrayList<>();
        initMarkersOptions.add(getStartMarkerOptions(new LatLng(route.getPoints().get(0).getLatitude(), route.getPoints().get(0).getLongitude())));
        initMarkersOptions.add(getEndMarkerOptions(new LatLng(route.getPoints().get(route.getPoints().size() - 1).getLatitude(), route.getPoints().get(route.getPoints().size() - 1).getLongitude())));
        for(int i = 1; i < route.getPoints().size() - 1; i++) {
            initMarkersOptions.add(initMarkersOptions.size()-1, getMarkerOption(new LatLng(route.getPoints().get(i).getLatitude(), route.getPoints().get(i).getLongitude())));
        }
        mapRouteComponent.initMap(getSupportFragmentManager(), initMarkersOptions);

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverDetailsActivity.this, DriverCriteriaActivity.class);
                intent.putExtra(C.IntentKeys.ROUTE_EDIT, route);
                startActivityForResult(intent, C.RequestCodes.NEW_FLOW);
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Void> call = restService.deleteRoute(route.getId());
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        setResult(C.ResultCodes.SUCCESS);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == C.RequestCodes.NEW_FLOW && resultCode == C.ResultCodes.SUCCESS) {
            setResult(C.ResultCodes.SUCCESS, data);
            finish();
        }
    }

    public MarkerOptions getStartMarkerOptions(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt startowy");
    }

    public MarkerOptions getEndMarkerOptions(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt końcowy")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
    }


    public MarkerOptions getMarkerOption(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt " + Character.toString((char) (mapRouteComponent.getMarkers().size() + 64)))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
    }


}
