package com.replicators.letscode2015.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.driver.adapter.RouteListAdapter;
import com.replicators.letscode2015.base.api.domain.RestRoute;
import com.replicators.letscode2015.base.api.domain.ListRouteResponse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverRouteListActivity extends BaseActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.activity_driver_route_list_button)
    View addButton;

    RouteListAdapter routeListAdapter;

    private ArrayList<RestRoute> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_route_list);

        ButterKnife.bind(this);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        Date date = Calendar.getInstance().getTime();

        routeListAdapter = new RouteListAdapter(list, this);
        recyclerView.setAdapter(routeListAdapter);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(DriverRouteListActivity.this, DriverCriteriaActivity.class), C.RequestCodes.NEW_FLOW);
            }
        });


        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverRouteListActivity.this, DriverDetailsActivity.class);
            }
        });

        refreshList();


    }

    private void refreshList() {
        Call<ListRouteResponse> call = restService.getRoutes();
        call.enqueue(new Callback<ListRouteResponse>() {
            @Override
            public void onResponse(Call<ListRouteResponse> call, Response<ListRouteResponse> response) {
                list.clear();
                for (RestRoute route : response.body().getRouteResponseList()) {
                    list.add(route);
                }
                routeListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ListRouteResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == C.RequestCodes.NEW_FLOW && resultCode == C.ResultCodes.SUCCESS) {
            refreshList();
        }
    }
}
