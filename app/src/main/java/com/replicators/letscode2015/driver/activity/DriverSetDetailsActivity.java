package com.replicators.letscode2015.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.api.domain.RestRoute;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by taiga on 11.11.16.
 */

public class DriverSetDetailsActivity extends BaseActivity {
    @Bind(R.id.activity_drivers_details_route_title)
    EditText routeName;
    @Bind(R.id.activity_drivers_details_free_places)
    EditText freePlaces;
    @Bind(R.id.activity_drivers_details_describe)
    EditText describe;


    private View buttonNext;
    private RestRoute routeRequest = new RestRoute();
    private RestRoute editedRoute;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drivers_set_details);
        ButterKnife.bind(this);
        routeRequest = (RestRoute) getIntent().getSerializableExtra(C.IntentKeys.ROUTE_REQUEST);
        initializeActionBar();

        if(getIntent() != null) {
            editedRoute = (RestRoute) getIntent().getSerializableExtra(C.IntentKeys.ROUTE_EDIT);
            if(editedRoute != null) {
                routeName.setText(editedRoute.getRouteName());
                describe.setText(editedRoute.getDescribe());
                freePlaces.setText(Integer.toString(editedRoute.getFreePlaces()));
            }
        }
    }

    public void initializeActionBar() {
        LayoutInflater inflater = (LayoutInflater) getSupportActionBar()
                .getThemedContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View customActionBarView = inflater.inflate(R.layout.actionbar_driver_describe, null);
        buttonNext = customActionBarView.findViewById(R.id.button_next_driver);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeRequest.setRouteName(routeName.getText().toString());
                routeRequest.setFreePlaces(Integer.valueOf(freePlaces.getText().toString()));
                routeRequest.setDescribe(describe.getText().toString());
                if(editedRoute == null) {
                    Call<Void> call = restService.addRoute(routeRequest);
                    call.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Intent intent = new Intent();
                            setResult(C.ResultCodes.SUCCESS, intent);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                        }
                    });
                } else {
                    routeRequest.setId(editedRoute.getId());
                    routeRequest.setUserId(editedRoute.getUserId());
                    Call<Void> call = restService.editRoute(routeRequest);
                    call.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Intent intent = new Intent();
                            setResult(C.ResultCodes.SUCCESS, intent);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                        }
                    });
                }

            }
        });


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
    }


}
