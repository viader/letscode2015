package com.replicators.letscode2015.driver.listener;

/**
 * Created by taiga on 11.11.16.
 */

public interface DriverRoutesComponentListener {
    void onPointsNumberChanged(int pointsNumber);
}
