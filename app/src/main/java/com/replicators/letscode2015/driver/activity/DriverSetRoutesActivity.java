package com.replicators.letscode2015.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.driver.ui.DriverRoutesComponent;
import com.replicators.letscode2015.base.ui.MapRouteComponent;
import com.replicators.letscode2015.driver.listener.DriverRoutesComponentListener;
import com.replicators.letscode2015.base.api.domain.RestPoint;
import com.replicators.letscode2015.base.api.domain.RestRoute;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DriverSetRoutesActivity extends BaseActivity implements DriverRoutesComponentListener {

    @Bind(R.id.activity_driver_set_route_view)
    DriverRoutesComponent setRouteView;
    @Bind(R.id.map)
    MapRouteComponent mapRouteComponent;
    private View buttonNext;
    private RestRoute routeRequest = new RestRoute();

    private RestRoute editedRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_driver_set_route);
        ButterKnife.bind(this);
        setRouteView.setListener(this);
        initializeActionBar();
        routeRequest = (RestRoute) getIntent().getSerializableExtra(C.IntentKeys.ROUTE_REQUEST);

        if(getIntent() != null) {
            editedRoute = (RestRoute) getIntent().getSerializableExtra(C.IntentKeys.ROUTE_EDIT);
        }

        ArrayList<MarkerOptions> initMarkersOptions = new ArrayList<>();
        if(editedRoute == null) {
            initMarkersOptions.add(getStartMarkerOptions(new LatLng(51.254735, 22.549887)));
            initMarkersOptions.add(getEndMarkerOptions(new LatLng(51.273417, 22.603655)));
        } else {
            initMarkersOptions.add(getStartMarkerOptions(new LatLng(editedRoute.getPoints().get(0).getLatitude(), editedRoute.getPoints().get(0).getLongitude())));
            initMarkersOptions.add(getEndMarkerOptions(new LatLng(editedRoute.getPoints().get(editedRoute.getPoints().size() - 1).getLatitude(), editedRoute.getPoints().get(editedRoute.getPoints().size() - 1).getLongitude())));
            for(int i = 1; i < editedRoute.getPoints().size() - 1; i++) {
                initMarkersOptions.add(initMarkersOptions.size() - 1, getMarkerOption(new LatLng(editedRoute.getPoints().get(i).getLatitude(), editedRoute.getPoints().get(i).getLongitude())));
            }
            setRouteView.setupPointsNumber(editedRoute.getPoints().size());
        }
            mapRouteComponent.initMap(getSupportFragmentManager(), initMarkersOptions);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == C.RequestCodes.NEW_FLOW && resultCode == C.ResultCodes.SUCCESS) {
            setResult(C.ResultCodes.SUCCESS, data);
            finish();
        }
    }

    public void initializeActionBar() {
        final LayoutInflater inflater = (LayoutInflater) getSupportActionBar()
                .getThemedContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View customActionBarView = inflater.inflate(R.layout.actionbar_driver_map, null);
        buttonNext = customActionBarView.findViewById(R.id.button_next_driver);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<RestPoint> pointsList = new ArrayList<>();

                for (Marker marker : mapRouteComponent.getMarkers()) {
                    pointsList.add(new RestPoint(marker.getPosition()));
                }
                routeRequest.setPoints(pointsList);
                Intent intent = new Intent(DriverSetRoutesActivity.this, DriverSetDetailsActivity.class);
                intent.getSerializableExtra(C.IntentKeys.ROUTE_REQUEST);
                intent.putExtra(C.IntentKeys.ROUTE_REQUEST, routeRequest);
                intent.putExtra(C.IntentKeys.ROUTE_EDIT, editedRoute);
                startActivityForResult(intent, C.RequestCodes.NEW_FLOW);

            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));

    }


    @Override
    public void onPointsNumberChanged(int pointsNumber) {
        if (pointsNumber > mapRouteComponent.getMarkers().size()) {
            mapRouteComponent.addMarker(getMarkerOption(mapRouteComponent.getCameraPosition()), mapRouteComponent.getMarkers().size() - 1);
        } else {
            mapRouteComponent.removeMarker(mapRouteComponent.getMarkers().size() - 2);
        }
        if (pointsNumber > 2)
            buttonNext.setVisibility(View.VISIBLE);
        else
            buttonNext.setVisibility(View.GONE);
    }


    public MarkerOptions getStartMarkerOptions(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt startowy");
    }

    public MarkerOptions getEndMarkerOptions(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt końcowy")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
    }

    public MarkerOptions getMarkerOption(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt " + Character.toString((char) (mapRouteComponent.getMarkers().size() + 64)))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
    }


}
