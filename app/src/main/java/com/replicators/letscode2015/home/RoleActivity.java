package com.replicators.letscode2015.home;

import android.content.Intent;
import android.os.Bundle;

import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.api.domain.RestUser;
import com.replicators.letscode2015.base.api.domain.UserProfileRequest;
import com.replicators.letscode2015.base.api.domain.UserToOpinionResponse;
import com.replicators.letscode2015.chat.ChatListActivity;
import com.replicators.letscode2015.chat.domain.ChatResponse;
import com.replicators.letscode2015.driver.activity.DriverRouteListActivity;
import com.replicators.letscode2015.passenger.activity.PassengerCriteriaActivity;
import com.replicators.letscode2015.profile.UserProfileActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RoleActivity extends BaseActivity {


    RestUser restUser = new RestUser();



    @OnClick(R.id.button_passenger)
    public void onClickPassenger() {
        startActivity(new Intent(this, PassengerCriteriaActivity.class));
    }

    @OnClick(R.id.button_driver)
    public void onClickDriver() {
        startActivity(new Intent(this, DriverRouteListActivity.class));
    }

    @OnClick(R.id.chat_button)
    public void onClickChat() {
        startActivity(new Intent(this, ChatListActivity.class));
    }

    @OnClick(R.id.button_profil)
    public void onClickProfile() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra(C.IntentKeys.USER_REQUEST, restUser.getUserId());
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_role);
        ButterKnife.bind(this);
    }
}
