package com.replicators.letscode2015.start;

import android.content.Intent;
import android.os.Bundle;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.login.activity.LoginActivity;
import com.replicators.letscode2015.register.activity.RegisterActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by taiga on 13.10.16.
 */
public class StartActivity extends BaseActivity {

    @OnClick(R.id.button_login)
    public void onClickLogin() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @OnClick(R.id.button_register)
    public void onClickRegister() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
    }
}
