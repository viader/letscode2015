package com.replicators.letscode2015;

public class C {

    public interface IntentKeys {
        String USER_OPINION_REQUEST = "USER_OPINION_REQUEST";
        String ROUTE_REQUEST = "ROUTE_REQUEST";
        String REST_ROUTE = "REST_ROUTE";
        String ROUTE_EDIT = "ROUTE_EDIT";
        String KEY_DRIVER = "KEY_DRIVER";
        String ROUTE_FOR_PASSENGER = "ROUTE_FOR_PASSENGER";
        String USER_REQUEST = "USER_REQUEST";
    }

    public interface ResultCodes {
        int SUCCESS = 1;
        int FAIL = 0;
    }

    public interface RequestCodes {
        int NEW_FLOW = 1;
    }

    public interface Server {
        //192.168.0.47
        //192.168.0.115
        //212.182.18.236
        //192.168.1.104
        String ADDRESS = "http://192.168.0.44:8080/";
    }

    public interface ChatWebSocket {
        String SERVER = "ws://192.168.0.45:8080/mobilnyAutostop/chat/websocket";
        int TIMEOUT = 5000;
    }

}
