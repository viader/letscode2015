package com.replicators.letscode2015.profile;

import com.replicators.letscode2015.base.api.domain.OpinionResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class ProfileUserResponse implements Serializable {
    private int id;
    private String userName;
    private float driverMark;
    private float passengerMark;
    private List<OpinionResponse> opinions = new ArrayList<>();

    public ProfileUserResponse() {
    }

    public ProfileUserResponse(int id, String userName, float driverMark, float passengerMark, List<OpinionResponse> opinionResponses) {
        this.id = id;
        this.userName = userName;
        this.driverMark = driverMark;
        this.passengerMark = passengerMark;
        this.opinions = opinionResponses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public float getDriverMark() {
        return driverMark;
    }

    public void setDriverMark(float driverMark) {
        this.driverMark = driverMark;
    }

    public float getPassengerMark() {
        return passengerMark;
    }

    public void setPassengerMark(float passengerMark) {
        this.passengerMark = passengerMark;
    }

    public List<OpinionResponse> getOpinions()
    {
        return opinions;
    }

    public void setOpinions(List<OpinionResponse> pOpinions)
    {
        opinions = pOpinions;
    }
}
