package com.replicators.letscode2015.profile;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.api.domain.OpinionResponse;
import com.replicators.letscode2015.base.api.domain.RestUser;
import com.replicators.letscode2015.base.api.domain.UserProfileRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends BaseActivity {
    @Bind(R.id.activity_user_profile_user_name)
    TextView userName;
    @Bind(R.id.activity_user_profile_driver_marks)
    TextView driverMark;
    @Bind(R.id.activity_user_profile_passenger_marks)
    TextView passengerMark;
    @Bind(R.id.activity_user_profile_comment)
    ListView comment;

    RestUser restUser = new RestUser();
    private List<OpinionResponse> list = new ArrayList<>();
    private SwingBottomInAnimationAdapter adapter;
    OpinionResponse opinionResponse = new OpinionResponse();
    //UserProfileRequest userProfileRequest = new UserProfileRequest();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        restUser = (RestUser) getIntent().getSerializableExtra(C.IntentKeys.KEY_DRIVER);
        OpinionAdapter opinionAdapter = new OpinionAdapter(this, list);
        adapter = new SwingBottomInAnimationAdapter(opinionAdapter);
        adapter.setAbsListView(comment);
        adapter.getViewAnimator().setInitialDelayMillis(500);
        comment.setAdapter(adapter);
        comment.setSelector(android.R.color.transparent);
        Call<ProfileUserResponse> call = restService.getUserProfile(new UserProfileRequest(3));
        call.enqueue(new Callback<ProfileUserResponse>() {
            @Override
            public void onResponse(Call<ProfileUserResponse> call, Response<ProfileUserResponse> response) {
                list.clear();
                list.add(new OpinionResponse("Basia", "Dupa dupie dupa", "22:22"));
                userName.setText(response.body().getUserName());
                driverMark.setText(Float.toString(response.body().getDriverMark()));
                passengerMark.setText(Float.toString(response.body().getPassengerMark()));
                for(OpinionResponse opinionResponse : response.body().getOpinions()) {
                    list.add(opinionResponse);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ProfileUserResponse> call, Throwable t) {
                userName.setText("DUPA");
            }
        });
    }
}
