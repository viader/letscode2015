package com.replicators.letscode2015.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.api.domain.OpinionResponse;

import java.util.List;


/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class OpinionAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;

    private List<OpinionResponse> data;
    private UserProfileActivity activity = null;
    Context context = null;

    public OpinionAdapter(UserProfileActivity activity, List<OpinionResponse> data) {
        this.data = data;
        this.context = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static void setViewHolder(final UserProfileActivity activity, final ViewHolder view, OpinionResponse opinionResponse) {
        view.authorName.setText(opinionResponse.getAuthorName());
        view.date.setText(opinionResponse.getDate());
        view.textComment.setText(opinionResponse.getTextComment());
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null) {
            view = inflater.inflate(R.layout.opinion_item, null);
            new ViewHolder(view);
        }
        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        setViewHolder(activity, viewHolder, data.get(i));
        return view;
    }


    public static class ViewHolder {

        public TextView authorName;
        public TextView textComment;
        public TextView date;

        public ViewHolder(View convertView) {
            convertView.setTag(this);
            authorName = (TextView)convertView.findViewById(R.id.opinion_author);
            textComment = (TextView)convertView.findViewById(R.id.opinion_text);
            date = (TextView)convertView.findViewById(R.id.opinion_date);
        }
    }
}
