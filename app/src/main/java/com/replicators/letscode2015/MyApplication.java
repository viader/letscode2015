package com.replicators.letscode2015;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.replicators.letscode2015.base.injection.AppModule;
import com.replicators.letscode2015.base.injection.DaggerNetComponent;
import com.replicators.letscode2015.base.injection.NetComponent;
import com.replicators.letscode2015.base.injection.NetModule;

public class MyApplication extends MultiDexApplication {

    private static Application instance;

    private NetComponent netComponent;

    public static Application getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;


        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(C.Server.ADDRESS))
                .build();
    }

    public NetComponent getNetComponent() {
        return netComponent;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
