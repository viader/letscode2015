package com.replicators.letscode2015.login.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.home.RoleActivity;
import com.replicators.letscode2015.login.api.LoginResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by taiga on 13.10.16.
 */
public class LoginActivity extends BaseActivity {

    @Bind(R.id.activity_login_id_login)
    EditText loginEditText;

    @Bind(R.id.activity_login_id_password)
    EditText passwordEditText;

    @OnClick(R.id.button_do_login)
    public void onClickDoLogin() {
        Call<LoginResponse> call = restService.login(loginEditText.getText().toString(), passwordEditText.getText().toString());
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();
                headerInfo.token = loginResponse.token;
                headerInfo.login = loginResponse.login;

                startActivity(new Intent(LoginActivity.this, RoleActivity.class));
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Niepoprawny login i/lub hasło",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }
}
