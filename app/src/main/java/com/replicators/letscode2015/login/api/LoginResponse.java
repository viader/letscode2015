package com.replicators.letscode2015.login.api;

/**
 * Created by taiga on 03.11.16.
 */

public class LoginResponse {

    public String login;

    public String token;

    public String message;
}
