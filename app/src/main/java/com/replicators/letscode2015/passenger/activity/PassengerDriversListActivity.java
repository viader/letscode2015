package com.replicators.letscode2015.passenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nhaarman.listviewanimations.ArrayAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.api.domain.ListRouteResponse;
import com.replicators.letscode2015.base.api.domain.RestRoute;
import com.replicators.letscode2015.passenger.adapter.DriversListAdapter;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.domain.Driver;
import com.replicators.letscode2015.base.domain.DriversResult;
import com.replicators.letscode2015.base.utils.DataUtils;
import com.replicators.letscode2015.passenger.domain.GetRouteForPassengerRequest;
import com.replicators.letscode2015.passenger.domain.RoutePointList;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassengerDriversListActivity extends BaseActivity {

    @Bind(R.id.list)
    ListView listView;

    private SwingBottomInAnimationAdapter adapter;

    //private List<Driver> list;
    private List<RestRoute> list = new ArrayList<>();
    private GetRouteForPassengerRequest routeForPassengerRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger_drivers_list);
        ButterKnife.bind(this);
        routeForPassengerRequest = (GetRouteForPassengerRequest)getIntent().getSerializableExtra(C.IntentKeys.ROUTE_FOR_PASSENGER);
        DriversListAdapter cityAdapter = new DriversListAdapter(this, list);
        adapter = new SwingBottomInAnimationAdapter(cityAdapter);
        adapter.setAbsListView(listView);
        adapter.getViewAnimator().setInitialDelayMillis(500);
        listView.setAdapter(adapter);
        listView.setSelector(android.R.color.transparent);
        Call <ListRouteResponse> call = restService.getRoutesForPassenger(routeForPassengerRequest);
        call.enqueue(new Callback<ListRouteResponse>() {
            @Override
            public void onResponse(Call<ListRouteResponse> call, Response<ListRouteResponse> response) {
                list.clear();
                for(RestRoute restRoute : response.body().getRouteResponseList()) {
                    list.add(restRoute);
                }
            }

            @Override
            public void onFailure(Call<ListRouteResponse> call, Throwable t) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            Intent intent = new Intent(PassengerDriversListActivity.this, PassengerDriverDetailsActivity.class);

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                intent.putExtra(C.IntentKeys.KEY_DRIVER, list.get((int) l));
                startActivity(intent);
            }
        });
    }
}
