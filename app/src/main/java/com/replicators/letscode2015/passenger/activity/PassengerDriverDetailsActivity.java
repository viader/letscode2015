package com.replicators.letscode2015.passenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.api.domain.RestPoint;
import com.replicators.letscode2015.base.api.domain.RestRoute;
import com.replicators.letscode2015.chat.ChatActivity;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.ui.MapRouteComponent;
import com.replicators.letscode2015.base.domain.Driver;
import com.replicators.letscode2015.base.domain.LatitudeLongitude;
import com.replicators.letscode2015.chat.domain.ChatResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PassengerDriverDetailsActivity extends BaseActivity {

    @Bind(R.id.route_name)
    TextView nameTextView;
    @Bind(R.id.describe)
    TextView describeTextView;
    @Bind(R.id.hours)
    TextView hoursTextView;
    @Bind(R.id.rating)
    TextView ratingTextView;
    @Bind(R.id.free_places)
    TextView freePlacesTextView;
    @Bind(R.id.activity_after_register_button_next)
    View chatButton;
    @Bind(R.id.opinion)
    View opinionButton;
    @Bind(R.id.map)
    MapRouteComponent mapRouteComponent;
    private GoogleMap map;
    private RestRoute route = null;
    //private ChatResponse
    private List<RestPoint> routePoints = null;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        route = (RestRoute) getIntent().getSerializableExtra(C.IntentKeys.KEY_DRIVER);
        routePoints = route.getPoints();

        setContentView(R.layout.activity_passenger_drivers_details);
        ButterKnife.bind(this);

        nameTextView.setText(route.getRouteName());
        describeTextView.setText(route.getDescribe());
        hoursTextView.setText(sdf.format(route.getStartTime()) + "-" + sdf.format(route.getEndTime()));
        ratingTextView.setText(getString(R.string.rating) + " " + 5);
        freePlacesTextView.setText(route.getFreePlaces() + " " + getString(R.string.free_places));

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                startActivity(new Intent(PassengerDriverDetailsActivity.this, ChatActivity.class));
            }
        });

        opinionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(PassengerDriverDetailsActivity.this, MarkCommentActivity.class);
                intent.putExtra(C.IntentKeys.KEY_DRIVER, route.getUserId());
                startActivity(intent);
            }
        });


        ArrayList<MarkerOptions> initMarkersOptions = new ArrayList<>();
        for (int i = 0; i < routePoints.size(); i++) {
            initMarkersOptions.add(new MarkerOptions().position(new LatLng(routePoints.get(i).getLatitude(), routePoints.get(i).getLongitude())));
        }
        mapRouteComponent.initMap(getSupportFragmentManager(), initMarkersOptions);
    }


}
