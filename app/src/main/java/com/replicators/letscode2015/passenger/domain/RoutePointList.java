package com.replicators.letscode2015.passenger.domain;

import com.replicators.letscode2015.base.domain.LatitudeLongitude;

import java.util.ArrayList;
import java.util.List;

public class RoutePointList {
    private List<LatitudeLongitude> routePoints = new ArrayList<>();

    public RoutePointList(List<LatitudeLongitude> routePoints) {
        this.routePoints = routePoints;
    }
}
