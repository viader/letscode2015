package com.replicators.letscode2015.passenger.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.api.domain.RestRoute;
import com.replicators.letscode2015.base.domain.Driver;

import java.util.List;

public class DriversListAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;

    private List<RestRoute> data;

    private Context context = null;

    public DriversListAdapter(Activity activity, List<RestRoute> data) {
        this.data = data;
        this.context = activity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static void setViewHolder(Context context, final ViewHolder view, RestRoute route) {
        view.name.setText(route.getRouteName());
        view.ratingBar.setRating(4);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public RestRoute getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.driver_list_item, null);
            new ViewHolder(convertView);
        }
        final ViewHolder view = (ViewHolder) convertView.getTag();
        setViewHolder(context, view, data.get(position));
        return convertView;
    }

    public static class ViewHolder {

        public TextView name;

        public RatingBar ratingBar;

        public ViewHolder(View convertView) {
            convertView.setTag(this);
            name = (TextView) convertView.findViewById(R.id.route_name);
            ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
        }
    }
}
