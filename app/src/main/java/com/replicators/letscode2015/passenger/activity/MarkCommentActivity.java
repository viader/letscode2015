package com.replicators.letscode2015.passenger.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.api.domain.RestUser;
import com.replicators.letscode2015.base.api.domain.UserToOpinionRequest;
import com.replicators.letscode2015.base.api.domain.UserToOpinionResponse;
import com.replicators.letscode2015.base.api.domain.RestOpinion;
import com.replicators.letscode2015.passenger.ui.AddedBlackListDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class MarkCommentActivity extends BaseActivity {
    @Bind(R.id.activity_mark_comment_user_name)
    TextView userName;
    @Bind(R.id.activity_mark_comment_rating)
    RatingBar ratingBar;
    @Bind(R.id.activity_mark_comment_comment)
    EditText comment;
    @Bind(R.id.activity_mark_comment_black_list)
    View blackList;
    @Bind(R.id.activity_mark_comment_add_opinion)
    Button addOpinion;

    private RestOpinion opinionRequest = new RestOpinion();
    private RestUser userRequest = new RestUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_comment);
        ButterKnife.bind(this);

        UserToOpinionRequest userToOpinion = (UserToOpinionRequest) getIntent().getSerializableExtra(C.IntentKeys.USER_OPINION_REQUEST);
        Call<UserToOpinionResponse> call = restService.getUserToOpinion(userToOpinion);
        call.enqueue(new Callback<UserToOpinionResponse>() {
            @Override
            public void onResponse(Call<UserToOpinionResponse> call, Response<UserToOpinionResponse> response) {
                userName.setText( response.body().getName());
                opinionRequest.setUserId(response.body().getId());
                userRequest.setUserId(response.body().getId());
            }

            @Override
            public void onFailure(Call<UserToOpinionResponse> call, Throwable t) {

            }
        });
        initialize();
    }

    public void initialize() {
        addOpinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opinionRequest.setMarks(ratingBar.getRating());
                opinionRequest.setComment(comment.getText().toString());
                Call<Void> call = restService.addOpinion(opinionRequest);
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {

                    }
                });
            }


        });

        blackList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call <Void> call = restService.addToBlackList(userRequest);
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        AddedBlackListDialog dialog = new AddedBlackListDialog(MarkCommentActivity.this);
                        dialog.show();

                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {

                    }
                });
            }
        });
    }
}