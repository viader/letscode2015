package com.replicators.letscode2015.passenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.api.domain.RestPoint;
import com.replicators.letscode2015.base.ui.MapRouteComponent;
import com.replicators.letscode2015.passenger.domain.GetRouteForPassengerRequest;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PassengerMapsActivity extends BaseActivity {

    boolean isStarActive = false;
    @Bind(R.id.map)
    MapRouteComponent mapRouteComponent;
    private TextView actionBarText = null;
    private View buttonNext = null;
    private GetRouteForPassengerRequest routeForPassengerRequest = new GetRouteForPassengerRequest();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActionBar();
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        routeForPassengerRequest = (GetRouteForPassengerRequest)getIntent().getSerializableExtra(C.IntentKeys.ROUTE_FOR_PASSENGER);
        mapRouteComponent.initMap(getSupportFragmentManager(), new ArrayList<MarkerOptions>());
        mapRouteComponent.setMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (mapRouteComponent.getMarkers().size() == 0)
                    mapRouteComponent.addMarker(getStartMarkerOptions(latLng));
                else if (mapRouteComponent.getMarkers().size() == 1) {
                    mapRouteComponent.addMarker(getEndMarkerOptions(latLng));
                    actionBarText.setText("Przejdź dalej");
                    buttonNext.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == C.RequestCodes.NEW_FLOW && resultCode == C.ResultCodes.SUCCESS) {
            setResult(C.ResultCodes.SUCCESS, data);
            finish();
        }
    }

    public void initializeActionBar() {
        final LayoutInflater inflater = (LayoutInflater) getSupportActionBar()
                .getThemedContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View customActionBarView = inflater.inflate(R.layout.actionbar_passenger_map, null);
        actionBarText = (TextView) customActionBarView.findViewById(R.id.action_bar_title);
        final ImageView star = (ImageView) customActionBarView.findViewById(R.id.star);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isStarActive && mapRouteComponent.getMarkers().size() == 2) {
                    star.setBackground(getResources().getDrawable(R.drawable.star_active));
                    isStarActive = true;
                } else {
                    star.setBackground(getResources().getDrawable(R.drawable.star_unactive));
                    isStarActive = false;
                }
            }
        });
        final ImageView hint = (ImageView) customActionBarView.findViewById(R.id.hint);
        hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(R.string.title_hint_one, R.string.hint_one);
            }
        });
        buttonNext = (View) customActionBarView.findViewById(R.id.activity_after_register_button_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Marker markerStart = mapRouteComponent.getMarkers().get(0);
                Marker markerEnd = mapRouteComponent.getMarkers().get(mapRouteComponent.getMarkers().size() - 1);
                RestPoint startPoint = new RestPoint(markerStart.getPosition());
                RestPoint endPoint = new RestPoint(markerEnd.getPosition());

                Intent intent = new Intent(PassengerMapsActivity.this,
                        PassengerDriversListActivity.class);
                routeForPassengerRequest.startPoint = startPoint;
                routeForPassengerRequest.endPoint = endPoint;
                intent.getSerializableExtra(C.IntentKeys.ROUTE_FOR_PASSENGER);
                intent.putExtra(C.IntentKeys.ROUTE_FOR_PASSENGER, routeForPassengerRequest);
                startActivityForResult(intent, C.RequestCodes.NEW_FLOW);
            }
        });
        actionBarText.setText("Wybierz trasę.");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
    }


    public MarkerOptions getStartMarkerOptions(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt startowy");
    }

    public MarkerOptions getEndMarkerOptions(LatLng point) {
        return new MarkerOptions().position(
                new LatLng(point.latitude, point.longitude))
                .draggable(true)
                .title("Punkt końcowy")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
    }

}
