package com.replicators.letscode2015.passenger.domain;

import com.replicators.letscode2015.base.api.domain.RestPoint;

import java.io.Serializable;
import java.util.Date;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class GetRouteForPassengerRequest implements Serializable {
    public RestPoint startPoint;
    public RestPoint endPoint;
    public Date startTime;
    public Date endTime;
}
