package com.replicators.letscode2015.passenger.activity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.base.ui.TimePickerFragment;
import com.replicators.letscode2015.passenger.domain.GetRouteForPassengerRequest;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PassengerCriteriaActivity extends BaseActivity implements TimePickerDialog.OnTimeSetListener {

    @Bind(R.id.text_start_time)
    TextView startTimeTextView;
    @Bind(R.id.text_end_time)
    TextView endTimeTextView;
    private boolean isStartTime = false;
    private Date startTime;
    private Date endTime;
    private View buttonNext;
    private GetRouteForPassengerRequest routeForPassengerRequest = new GetRouteForPassengerRequest();

    @OnClick(R.id.button_start_time)
    public void showStartTimePickerDialog(View view) {
        isStartTime = true;
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @OnClick(R.id.button_end_time)
    public void showEndTimePickerDialog(View view) {
        isStartTime = false;
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger_criteria);
        ButterKnife.bind(this);
        initializeActionBar();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == C.RequestCodes.NEW_FLOW && resultCode == C.ResultCodes.SUCCESS) {
            setResult(C.ResultCodes.SUCCESS, data);
            finish();
        }
    }

    public void initializeActionBar() {
        LayoutInflater inflater = (LayoutInflater) getSupportActionBar()
                .getThemedContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View customActionBarView = inflater.inflate(R.layout.actionbar_passenger_time, null);
        buttonNext = customActionBarView.findViewById(R.id.activity_after_register_button_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeForPassengerRequest.startTime = startTime;
                routeForPassengerRequest.endTime = endTime;
                Intent intent = new Intent(PassengerCriteriaActivity.this, PassengerMapsActivity.class);
                intent.putExtra(C.IntentKeys.ROUTE_FOR_PASSENGER, routeForPassengerRequest);
                startActivityForResult(intent, C.RequestCodes.NEW_FLOW);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        String time = String.format("%02d:%02d", hourOfDay, minute);
        if (isStartTime) {
            startTimeTextView.setText(getText(R.string.set_start_time_button) + " " + time);
            startTime = calendar.getTime();
        } else {
            endTimeTextView.setText(getText(R.string.set_end_time_button) + " " + time);
            endTime = calendar.getTime();
        }
        if (startTime != null && endTime != null && !(startTime.after(endTime))) {
            buttonNext.setVisibility(View.VISIBLE);
        }

    }
}
