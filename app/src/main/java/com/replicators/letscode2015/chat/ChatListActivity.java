package com.replicators.letscode2015.chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.chat.domain.ChatListResponse;
import com.replicators.letscode2015.chat.domain.ChatResponse;
import com.replicators.letscode2015.chat.model.ChatMessage;
import com.replicators.letscode2015.passenger.activity.MarkCommentActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class ChatListActivity extends BaseActivity {
    @Bind(R.id.activity_chat_list)
    ListView chatList;
    List<ChatResponse> chats = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        ButterKnife.bind(this);
        final ChatListAdapter chatListAdapter = new ChatListAdapter(chats, this);
        chatList.setAdapter(chatListAdapter);

        Call <ChatListResponse> call = restService.getChatList();
        call.enqueue(new Callback<ChatListResponse>() {
            @Override
            public void onResponse(Call<ChatListResponse> call, Response<ChatListResponse> response) {
                chats.clear();
                for(ChatResponse chatResponse : response.body().chats) {
                    chats.add(chatResponse);
                }
                chatListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ChatListResponse> call, Throwable t) {

            }
        });


        chatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ChatListActivity.this, ChatActivity.class);
                intent.putExtra("as", chats.get((int) l));
                startActivity(intent);
            }


        });
    }
}
