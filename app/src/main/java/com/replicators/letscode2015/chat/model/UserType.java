package com.replicators.letscode2015.chat.model;

/**
 * Created by madhur on 17/01/15.
 */
public enum UserType {
    SELF, OTHER
};
