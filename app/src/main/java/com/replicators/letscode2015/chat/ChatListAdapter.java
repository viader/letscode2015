package com.replicators.letscode2015.chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.replicators.letscode2015.C;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.api.domain.UserToOpinionRequest;
import com.replicators.letscode2015.chat.domain.ChatResponse;
import com.replicators.letscode2015.passenger.activity.MarkCommentActivity;

import java.util.List;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class ChatListAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;

    private List<ChatResponse> data;

    private ChatListActivity activity = null;

    private TextView addOpinion;

    public ChatListAdapter(List<ChatResponse> data, ChatListActivity activity) {
        this.data = data;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static void setViewHolder(final ChatListActivity activity, final ChatListAdapter.ViewHolder view, final ChatResponse chat) {
        view.name.setText(chat.getName());
        view.date.setText(chat.getDate());
        view.addOpinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MarkCommentActivity.class);
                intent.putExtra(C.IntentKeys.USER_OPINION_REQUEST, new UserToOpinionRequest(chat.getName()));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.chat_list_item, null);
            new ViewHolder(convertView);
        }
        final ViewHolder view = (ViewHolder) convertView.getTag();
        setViewHolder(activity, view, data.get(i));
        return convertView;
    }

    public static class ViewHolder {

        public TextView name;
        public TextView date;
        public TextView addOpinion;

        public ViewHolder(View convertView) {
            convertView.setTag(this);
            name = (TextView) convertView.findViewById(R.id.chat_name);
            date = (TextView) convertView.findViewById(R.id.chat_date);
            addOpinion = (TextView) convertView.findViewById(R.id.activity_chat_list_add_opinion);
        }
    }
}
