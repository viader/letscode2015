package com.replicators.letscode2015.register.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by taiga on 13.10.16.
 */
public class RegisterActivity extends BaseActivity {

    @Bind(R.id.activity_register_id_login)
    EditText loginEditText;

    @Bind(R.id.activity_register_id_password)
    EditText passwordEditText;

    @Bind(R.id.activity_register_id_email)
    EditText emailEditText;

    @Bind(R.id.activity_register_id_phone)
    EditText phoneEditText;

    @OnClick(R.id.activity_register_id_button_register)
    public void onClickRegister() {
        Call<Void> call = restService.register(emailEditText.getText().toString(), loginEditText.getText().toString(), passwordEditText.getText().toString());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful())
                    startActivity(new Intent(RegisterActivity.this, AfterRegisterActivity.class));
                else {
                    Toast.makeText(RegisterActivity.this, "User istnieje",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "User istnieje",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }
}
