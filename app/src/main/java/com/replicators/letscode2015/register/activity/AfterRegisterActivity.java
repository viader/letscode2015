package com.replicators.letscode2015.register.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.activity.BaseActivity;
import com.replicators.letscode2015.login.activity.LoginActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by taiga on 03.11.16.
 */

public class AfterRegisterActivity extends BaseActivity {

    @OnClick(R.id.activity_after_register_button_next)
    public void onClick(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_after_register);
        ButterKnife.bind(this);
    }

}
