package com.replicators.letscode2015.base.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.replicators.letscode2015.R;

public class IconImageView extends ImageView {

    private int color;

    public IconImageView(Context context) {
        super(context);
    }

    public IconImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public IconImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.IconImageView, defStyle, 0);
        color = a.getColor(R.styleable.IconImageView_icon_color, -1);
        a.recycle();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        updateColor();
    }

    private void updateColor() {
        setColorFilter(color);
        invalidate();
    }
}
