package com.replicators.letscode2015.base.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.replicators.letscode2015.R;
import com.replicators.letscode2015.base.ui.integration.HttpConnection;
import com.replicators.letscode2015.base.ui.integration.PathJSONParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;

public class MapRouteComponent extends LinearLayout implements OnMapReadyCallback {

    private GoogleMap map;
    private List<Marker> markers = new ArrayList<>();
    private Polyline route = null;
    private List<MarkerOptions> initMarkersOptions = new ArrayList<>();
    private GoogleMap.OnMapClickListener mapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {

        }
    };

    public MapRouteComponent(Context context) {
        this(context, null);
    }

    public MapRouteComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.map_route_component, this, true);
        ButterKnife.bind(this);


    }

    public void initMap(FragmentManager manager, List<MarkerOptions> initMarkersOptions) {
        this.initMarkersOptions.addAll(initMarkersOptions);
        SupportMapFragment mapFragment = (SupportMapFragment) manager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void setMapClickListener(GoogleMap.OnMapClickListener mapClickListener) {
        this.mapClickListener = mapClickListener;
    }

    public List<Marker> getMarkers() {
        return markers;
    }

    public void addMarker(MarkerOptions markerOptions) {
        Marker marker = map.addMarker(markerOptions);
        markers.add(marker);
        drawRoute();
    }

    public void addMarker(MarkerOptions markerOptions, int position) {
        Marker marker = map.addMarker(markerOptions);
        markers.add(position, marker);
        drawRoute();
    }

    public void removeMarker(int position) {
        markers.get(position).remove();
        markers.remove(position);
        drawRoute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        map.setOnMapClickListener(mapClickListener);

        for (int i = 0; i < initMarkersOptions.size(); i++) {
            Marker marker = map.addMarker(initMarkersOptions.get(i));
            markers.add(marker);
        }

        if (markers.size() > 2) {
            centerToPlace(map, markers.get(0).getPosition().latitude, markers.get(0).getPosition().longitude);
        } else {
            centerToCity(map);
        }

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                drawRoute();
            }
        });

        drawRoute();
    }


    public void centerToCity(GoogleMap googleMap) {
        LatLng city = new LatLng(51.250792, 22.572189);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(city)
                .zoom(12)
                .build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void centerToPlace(GoogleMap googleMap, double latitude, double longitude) {
        LatLng city = new LatLng(latitude, longitude);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(city)
                .zoom(12)
                .build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public LatLng getCameraPosition() {
        return map.getCameraPosition().target;
    }

    private void drawRoute() {
        if (route != null) {
            route.remove();
            route = null;
        }
        if (markers.size() < 2) {
            return;
        }
        String url = getMapsApiDirectionsUrl();
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);
    }

    private String getMapsApiDirectionsUrl() {
        String generalParameters = "origin="
                + markers.get(0).getPosition().latitude + "," + markers.get(0).getPosition().longitude
                + "&destination="
                + markers.get(markers.size() - 1).getPosition().latitude + ","
                + markers.get(markers.size() - 1).getPosition().longitude;

        String sensor = "sensor=false";
        String params = generalParameters + "&" + sensor;
        if (markers.size() > 2) {
            String waypoints = "waypoints=optimize:true";
            for (int i = 1; i < markers.size() - 1; i++) {
                waypoints = waypoints + "|" +
                        +markers.get(i).getPosition().latitude + "," + markers.get(i).getPosition().longitude;
            }
            params += "&" + waypoints;
        }

        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + params;

        Log.d("MAPA", url);
        return url;
    }

    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points;
            PolylineOptions polyLineOptions = null;
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(2);
                polyLineOptions.color(Color.BLUE);
            }
            if (polyLineOptions != null) {
                route = map.addPolyline(polyLineOptions);
            }
        }
    }
}
