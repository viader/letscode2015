package com.replicators.letscode2015.base.utils;

import android.content.Context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.replicators.letscode2015.base.domain.DriversResult;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.replicators.letscode2015.base.lib.Debug;


public class DataUtils {

    private static ObjectMapper mapper = new ObjectMapper();

    public static void saveDriversData(Context context, DriversResult data) {
        try {
            String stringData = mapper.writeValueAsString(data);
            writeToFile(context, "drivers.data", stringData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public static DriversResult getDriversData(Context context) {
        String stringData = readFromFile(context, "drivers.data");
        DriversResult data = null;
        try {
            data = mapper.readValue(stringData, DriversResult.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static String readFromFile(Context context, String filename) {
        String ret = "";
        try {
            InputStream inputStream = context.getAssets().open(filename);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString + "\r\n");
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Debug.debug("File not found: " + e.toString());
        } catch (IOException e) {
            Debug.debug("Can not read file: " + e.toString());
        }
        return ret;
    }

    private static void writeToFile(Context context, String filename, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Debug.debug("File write failed: " + e.toString());
        }
    }
}
