package com.replicators.letscode2015.base.api.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taiga on 17.11.16.
 */
public class ListRouteResponse {
    private List<RestRoute> routeResponseList = new ArrayList<>();

    public List<RestRoute> getRouteResponseList() {
        return routeResponseList;
    }

    public void setRouteResponseList(List<RestRoute> pRouteResponseList) {
        routeResponseList = pRouteResponseList;
    }
}
