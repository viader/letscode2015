package com.replicators.letscode2015.base.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2015-11-21.
 */
public class Route {

    private List<Coordinate> coordinates;

    public Route() {
        coordinates = new ArrayList<>();
    }

    public Route addCoordinate(Coordinate coordinate) {
        coordinates.add(coordinate);
        return this;
    }

    public void clearCoordinates() {
        coordinates = new ArrayList<>();
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }
}
