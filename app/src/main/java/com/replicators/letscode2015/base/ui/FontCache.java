package com.replicators.letscode2015.base.ui;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;


public class FontCache {

    private static FontCache instance;
    private HashMap<Integer, Typeface> cache = new HashMap<Integer, Typeface>();

    public static FontCache getInstance() {
        if (instance == null) {
            instance = new FontCache();
        }
        return instance;
    }

    public Typeface getTypeface(Context context, int enumOrdinal) {
        Typeface t = cache.get(enumOrdinal);
        if (t == null) {
            switch (enumOrdinal) {
                case 0:
                    t = Typeface.createFromAsset(context.getAssets(), "fonts/Trebuchet-Regular.ttf");
                    break;
                case 1:
                    t = Typeface.createFromAsset(context.getAssets(), "fonts/Trebuchet-Regular.ttf");
                    break;
                case 2:
                    t = Typeface.createFromAsset(context.getAssets(), "fonts/Trebuchet-Bold.ttf");
                    break;
                default:
                    t = Typeface.DEFAULT;
                    break;
            }
            cache.put(enumOrdinal, t);
        }
        return t;
    }
}
