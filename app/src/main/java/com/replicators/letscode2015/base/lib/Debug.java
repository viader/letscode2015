package com.replicators.letscode2015.base.lib;

import android.util.Log;

import com.replicators.letscode2015.BuildConfig;


/**
 * Similar to {@link android.util.Log} class, but the application name is
 * hardcoded for convenient use. Some output may be disabled in production
 * version to hide critical or secret messages. Verbose level can be changed by
 * modifying {@code VERBOSE_MODE} constant.
 */
public class Debug
{

	private static final String TAG = "mobilnyAutostop";

	public static final int NONE = 1;
	public static final int INFO = 2;
	public static final int DEBUG = 3;
	public static final int DEBUG_MORE = 4;

	/**
	 * Verbose level of application log. Eg. DEBUG level will show NONE, INFO
	 * and DEBUG messages, but not VERBOSE messages.
	 */
	private static final int MODE = BuildConfig.DEBUG ? DEBUG_MORE : NONE;



	/**
	 * Log VERBOSE message.
	 */
	public static void debugMore(String str)
	{
		if (getMode() >= DEBUG_MORE)
		{
			Log.v(TAG, str);
		}
	}



	/**
	 * Log DEBUG message.
	 */
	public static void debug(String str)
	{
		if (getMode() >= DEBUG)
		{
			Log.d(TAG, str);
		}
	}



	/**
	 * Log INFO message.
	 */
	public static void info(String str)
	{
		if (getMode() >= INFO)
		{
			Log.i(TAG, str);
		}
	}



	/**
	 * Log ERROR message. Error messages are always logged.
	 */
	public static void error(String string)
	{
		Log.e(TAG, string);
	}
	
	
	
	
	public static int getMode()
	{
		return MODE;
	}
}