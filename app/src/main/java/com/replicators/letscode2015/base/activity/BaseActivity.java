package com.replicators.letscode2015.base.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.replicators.letscode2015.base.injection.NetModule;
import com.replicators.letscode2015.MyApplication;
import com.replicators.letscode2015.base.ui.ConfirmationDialog;
import com.replicators.letscode2015.base.api.RestService;

import javax.inject.Inject;

public class BaseActivity extends AppCompatActivity {

    @Inject
    public RestService restService;
    @Inject
    public NetModule.HeaderInfo headerInfo;

    protected Dialog dialog = null;

    private boolean startActivityRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication) getApplication()).getNetComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        startActivityRunning = false;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void startActivity(Intent intent) {
        if (!startActivityRunning) {
            super.startActivity(intent);
            startActivityRunning = true;
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (!startActivityRunning) {
            super.startActivityForResult(intent, requestCode);
            startActivityRunning = true;
        }
    }

    public void showDialog(int titleId, int describeId) {
        dialog = new ConfirmationDialog(BaseActivity.this, titleId, describeId);
        dialog.show();
    }
}
