package com.replicators.letscode2015.base.domain;

/**
 * Created by user on 2015-11-21.
 */
public class Coordinate {

    private double x;

    private double y;

    public Coordinate(double x, double y) {
        this.setX(x);
        this.setY(y);
    }

    public double getX() {
        return x;
    }

    public Coordinate setX(double x) {
        this.x = x;
        return this;
    }

    public double getY() {
        return y;
    }

    public Coordinate setY(double y) {
        this.y = y;
        return this;
    }
}
