package com.replicators.letscode2015.base.utils;

import com.replicators.letscode2015.base.domain.Coordinate;
import com.replicators.letscode2015.base.domain.Route;

import java.util.List;

public class RouteFinder {

    public static Route getClosestRouteTo(Coordinate passengerSourceCoordinate, Coordinate passengerDestinationCoordinate, List<Route> routes) {
        Route result = routes.get(0);

        double routeSourceDistance = getClosestPointDistanceTo(passengerSourceCoordinate, routes.get(0));
        double routeDestinationDistance = getClosestPointDistanceTo(passengerSourceCoordinate, routes.get(0));

        double closestDistance = routeSourceDistance + routeDestinationDistance;

        for (Route route : routes) {
            routeSourceDistance = getClosestPointDistanceTo(passengerSourceCoordinate, route);
            routeDestinationDistance = getClosestPointDistanceTo(passengerSourceCoordinate, route);

            double currentDistance = routeSourceDistance + routeDestinationDistance;

            if (currentDistance < closestDistance) {
                result = route;
            }
        }

        return result;
    }

    private static double getClosestPointDistanceTo(Coordinate coordinate, Route route) {
        double closestDistance = getDistanceBetween(coordinate, route.getCoordinates().get(0));

        for (Coordinate routePoint : route.getCoordinates()) {
            double currentDistance = getDistanceBetween(coordinate, routePoint);

            if (currentDistance < closestDistance) {
                closestDistance = currentDistance;
            }
        }

        return closestDistance;
    }

    private static double getDistanceBetween(Coordinate first, Coordinate second) {
        double horizontalDistance = Math.abs(first.getX() - second.getX());
        double verticalDistance = Math.abs(first.getY() - second.getY());

        return (Math.sqrt(Math.pow(horizontalDistance, 2) + Math.pow(verticalDistance, 2)));
    }
}
