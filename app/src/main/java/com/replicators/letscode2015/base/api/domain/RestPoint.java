package com.replicators.letscode2015.base.api.domain;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by taiga on 11.11.16.
 */

public class RestPoint implements Serializable {
    private double latitude;
    private double longitude;

    public RestPoint(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public RestPoint(LatLng point) {
        latitude = point.latitude;
        longitude = point.longitude;
    }

    public RestPoint() {
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
