package com.replicators.letscode2015.base.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.replicators.letscode2015.R;

public class ConfirmationDialog extends Dialog {

    private Context context;

    private String title;

    private String describe;

    public ConfirmationDialog(final Context context) {
        // Set your theme here
        super(context, android.R.style.Theme_Black);
        this.context = context;

        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }
    }

    public ConfirmationDialog(final Context context, int title, int describe) {
        // Set your theme here
        super(context, android.R.style.Theme_Black);
        this.context = context;
        this.title = context.getString(title);
        this.describe = context.getString(describe);

        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }
    }

    public ConfirmationDialog(final Context context, String title, String describe) {
        // Set your theme here
        super(context, android.R.style.Theme_Black);
        this.context = context;
        this.title = title;
        this.describe = describe;

        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_confirmation);

        ((TextView) findViewById(R.id.title)).setText(title);
        ((TextView) findViewById(R.id.describe)).setText(describe);

        findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
