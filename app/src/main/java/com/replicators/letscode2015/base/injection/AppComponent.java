package com.replicators.letscode2015.base.injection;

import com.replicators.letscode2015.base.activity.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

    void inject(BaseActivity baseActivity);
}
