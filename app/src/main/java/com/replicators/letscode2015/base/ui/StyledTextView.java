package com.replicators.letscode2015.base.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.replicators.letscode2015.R;


public class StyledTextView extends TextView {

    public StyledTextView(Context context) {
        super(context);
    }

    public StyledTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(attrs);
    }

    public StyledTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(attrs);
    }

    private void setFont(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.StyledTextView, android.R.attr.textViewStyle, 0);
        int fontOrdinal = ta.getInt(R.styleable.StyledTextView_font, 0);
        ta.recycle();
        if (!isInEditMode()) {
            setTypeface(FontCache.getInstance().getTypeface(getContext(), fontOrdinal));
        }
    }
}
