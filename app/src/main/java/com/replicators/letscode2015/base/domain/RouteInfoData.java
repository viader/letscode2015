package com.replicators.letscode2015.base.domain;

import java.util.Date;

/**
 * Created by taiga on 20.10.16.
 */
public class RouteInfoData {

    private String routeName;

    private Date routeStartDate;

    private Date routeEndDate;

    public RouteInfoData(String routeName, Date routeStartDate, Date routeEndDate) {
        this.routeName = routeName;
        this.routeStartDate = routeStartDate;
        this.routeEndDate = routeEndDate;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public Date getRouteStartDate() {
        return routeStartDate;
    }

    public void setRouteStartDate(Date routeStartDate) {
        this.routeStartDate = routeStartDate;
    }

    public Date getRouteEndDate() {
        return routeEndDate;
    }

    public void setRouteEndDate(Date routeEndDate) {
        this.routeEndDate = routeEndDate;
    }
}
