package com.replicators.letscode2015.base.api.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by taiga on 11.11.16.
 */

public class RestRoute implements Serializable {

    private Date startTime;
    private Date endTime;
    private List<RestPoint> points;
    private String routeName;
    private String describe;
    private int freePlaces;
    private int id;
    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public List<RestPoint> getPoints() {
        return points;
    }

    public void setPoints(List<RestPoint> points) {
        this.points = points;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(int freePlaces) {
        this.freePlaces = freePlaces;
    }

}



