package com.replicators.letscode2015.base.domain;

import java.util.ArrayList;
import java.util.List;

public class DriversResult {

    private List<Driver> results;

    public DriversResult() {
        results = new ArrayList<>();
    }

    public DriversResult(List<Driver> results) {
        this.results = results;
    }

    public List<Driver> getResults() {
        if (results == null) {
            results = new ArrayList<>();
        }
        return results;
    }

    public void setResults(List<Driver> results) {
        this.results = results;
    }
}
