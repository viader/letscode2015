package com.replicators.letscode2015.base.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Driver implements Serializable {

    @JsonProperty("name")
    private String name;

    @JsonProperty("describe")
    private String describe;

    @JsonProperty("numberOfFreePlacesInCar")
    private int numberOfFreePlacesInCar;

    @JsonProperty("startTime")
    private Date startTime;

    @JsonProperty("endTime")
    private Date endTime;

    @JsonProperty("rating")
    private int rating = 0;

    @JsonProperty("routePoints")
    private List<LatitudeLongitude> routePoints = new ArrayList<>();

    @JsonProperty("id")
    private int id = 0;

    public Driver() {
    }

    public Driver(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public int getNumberOfFreePlacesInCar() {
        return numberOfFreePlacesInCar;
    }

    public void setNumberOfFreePlacesInCar(int numberOfFreePlacesInCar) {
        this.numberOfFreePlacesInCar = numberOfFreePlacesInCar;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<LatitudeLongitude> getRoutePoints() {
        return routePoints;
    }

    public void setRoutePoints(List<LatitudeLongitude> routePoints) {
        this.routePoints = routePoints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
