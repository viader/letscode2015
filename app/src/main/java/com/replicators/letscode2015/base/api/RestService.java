package com.replicators.letscode2015.base.api;

import com.replicators.letscode2015.base.api.domain.ListRouteResponse;
import com.replicators.letscode2015.base.api.domain.RestOpinion;
import com.replicators.letscode2015.base.api.domain.RestRoute;
import com.replicators.letscode2015.base.api.domain.RestUser;
import com.replicators.letscode2015.base.api.domain.UserProfileRequest;
import com.replicators.letscode2015.base.api.domain.UserToOpinionRequest;
import com.replicators.letscode2015.base.api.domain.UserToOpinionResponse;
import com.replicators.letscode2015.chat.domain.ChatListResponse;
import com.replicators.letscode2015.chat.domain.ChatRequest;
import com.replicators.letscode2015.chat.domain.ChatResponse;
import com.replicators.letscode2015.login.api.LoginResponse;
import com.replicators.letscode2015.passenger.domain.GetRouteForPassengerRequest;
import com.replicators.letscode2015.profile.ProfileUserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by taiga on 15.10.16.
 */
public interface RestService {

    @FormUrlEncoded
    @POST("mobilnyAutostop/api/register")
    Call<Void> register(@Field("email") String email,
                        @Field("login") String login,
                        @Field("passwordHash") String password);

    @FormUrlEncoded
    @POST("mobilnyAutostop/api/login")
    Call<LoginResponse> login(@Field("loginOrEmail") String loginOrEmail,
                              @Field("passwordHash") String password);

    @POST("mobilnyAutostop/api/addRoute")
    Call<Void> addRoute(@Body RestRoute routeRequest);

    @POST("mobilnyAutostop/api/getRoutes")
    Call<ListRouteResponse> getRoutes();

    @POST("mobilnyAutostop/api/addOpinion")
    Call<Void> addOpinion(@Body RestOpinion opinionRequest);

    @POST("mobilnyAutostop/api/editRoute")
    Call<Void> editRoute(@Body RestRoute routeRequest);

    @FormUrlEncoded
    @POST("mobilnyAutostop/api/deleteRoute")
    Call<Void> deleteRoute(@Field("id") int id);

    @POST("mobilnyAutostop/api/addToBlackList")
    Call<Void> addToBlackList(@Body RestUser userRequest);

    @POST("mobilnyAutostop/api/getRoutesForPassenger")
    Call<ListRouteResponse> getRoutesForPassenger(@Body GetRouteForPassengerRequest routeForPassengerRequest);

    @POST("mobilnyAutostop/api/getChatList")
    Call<ChatListResponse> getChatList();

    @POST("mobilnyAutostop/api/getUserToOpinion")
    Call<UserToOpinionResponse> getUserToOpinion(@Body UserToOpinionRequest userToOpinionRequest);

    @POST("mobilnyAutostop/api/getUserProfile")
    Call<ProfileUserResponse> getUserProfile(@Body UserProfileRequest request);

    @POST("mobilnyAutostop/api/getChat")
    Call<ChatResponse> getChat(@Body ChatRequest chatRequest);

}
